<?php 
    include_once('./../initialize.php'); 

    if(is_post_request()){
        $class = $_POST["class"];
        if(!isEmpty($class)){
            $class = string_to_class($class);
            echo $class::getDetailsFormFields();
        }
    }

?>