<?php

function make_url($path_to_file)
{
    if($path_to_file[0] != "/") {
        $path_to_file = "/" . $path_to_file;
    }
    return WWW_ROOT . $path_to_file;
}

function redirect_to($path_to_file)
{
    header("Location: " . make_url($path_to_file));
    exit;
}

function h($string) 
{
    $result = trim($string);
    $result = stripslashes($result);
    return htmlspecialchars($result);
}

function is_post_request()
{
    return ($_SERVER['REQUEST_METHOD'] === 'POST');
}


/**
 * string_to_class converts string to 
 *
 * @param  string $str
 * @param  string $namespace
 *
 * @return Product or child class instance
 */
function string_to_class(string $str ="Product", string $namespace = "Products\\")
{
    if(empty($str)) $str = "Product";
    return $namespace . str_replace('-','',$str);
}

function sanitize_input($bad_arr = [])
{
    $arr = [];
    foreach($bad_arr as $field => $value) {
        $arr[$field] = h($value);
    }
    return $arr;
}

/**
 * show_messages generates messages code block from session
 * and destroys old messages
 *
 * @return string
 */
function show_messages()
{
    $message_block = '';
    if(isset($_SESSION["messages"])) {
        $messages = $_SESSION["messages"];
        $message_block .= '<div class="messages">';
        // type can be success / warning 
        // used for different message colors
        foreach($messages as $msg => $type) {
            $message_block .= '<div class="message '. $type .'">';
            $message_block .= $msg;
            $message_block .= '</div>';
        }
        $message_block .= '</div>';

        unset($_SESSION["messages"]);

        return $message_block;
    }
}