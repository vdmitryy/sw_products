<?php 

function db_connect()
{
    $conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
    if($conn->errno){
        exit("Failed connec to database");
    } else return $conn;
}

function db_disconnect($conn)
{
    if(isset($conn)) $conn->close();
}

function isEmpty(string $string): bool
{
    return $string === "";
}

/**
 * minLength
 *
 * @param  string $string
 * @param  int $minLength
 *
 * @return bool true if $string >= $minLength
 */
function minLength(string $string, int $minLength = 0): bool
{
    return strlen($string) >= $minLength;
}

/**
 * maxLength
 * 
 * @param  string $string
 * @param  int $maxLength
 *
 * @return bool true if $string <= $manLength
 */
function maxLength(string $string, $maxLength = 200): bool
{
    return strlen($string) <= $maxLength;
}

/**
 * minInt
 *
 * @param  int $num
 * @param  int $minNum
 *
 * @return bool true if $num >= $minNum
 */
function minInt(int $num, int $minNum)
{
    return $num >= $minNum;
}

/**
 * maxInt
 *
 * @param  int $num
 * @param  int $maxNum
 *
 * @return bool true if $num <= $maxNum
 */
function maxInt(int $num, int $maxNum)
{
    return $num <= $maxNum;
}
