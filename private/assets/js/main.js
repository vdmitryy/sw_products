// on change of select
function getDetailsFields($el) {
    $.ajax({
        method: "POST",
        url: "./../../private/ajax/getDetailsFields.php",
        data: { 
            class: $el,
        }
    })
    .done( ($result) => {
        $('#detailed_info').html($result);
    });
}

// checkbox can be tuggled by clicking on product
function toggleCheckbox($el) {
    var checkbox = $($el).find('input[type="checkbox"]')
    checkbox.prop("checked", !checkbox.prop("checked"));
}

// displays additional fields after
// failed post request
$(document).ready( () => {
    var el = $('#typeSelector');
    if(el){
        el.change();
    }
});
