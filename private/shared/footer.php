    <footer>
        <div class="credits">
            <div class="author text-center">
                author : Dmitrijs Voronovs
            </div>
        </div>
    </footer>
</div>

<script src="<?php echo make_url("js/main.js"); ?>"></script>
</body>
</html>

<?php db_disconnect($db); ?>