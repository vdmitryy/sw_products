create table products(
    SKU nvarchar(20) PRIMARY KEY NOT NULL,
    Name nvarchar(150) not null,
    Price float(10,2) not null,
    Type ENUM ('Furniture','DVD-disc','Book') not null,
    Details nvarchar(20) not null,
    Created_at datetime not null default CURRENT_TIMESTAMP()
)