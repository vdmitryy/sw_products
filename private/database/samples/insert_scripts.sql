insert into products (SKU, Name, Price, Type, Details) values 
    ('FPR11A','Chair','115.1','Furniture','15x552x51'),
    ('DPR11A','50-cent - PIMP','512','DVD-disc','14500'),
    ('FPR21A','TV','144.555','Furniture','15x12x51'),
    ('DPR21A','Michael Jackson','55245.5','DVD-disc','1500'),
    ('BPR11A','About neighbours','551','Book','151'),
    ('BPR21A','Read it','5','Book','5551'),
    ('BPR31A','About me','155.6','Book','1100'),
    ('DPR31A','Wonderland','25.4','DVD-disc','500'),
    ('FPR31A','Sofa','5555','Furniture','515x5x100'),
    ('DPR41A','NIRVANA','25.4','DVD-disc','500'),
    ('DPR51A','50-cent - IN DA CLUB','52','DVD-disc','14500'),
    ('BPR41A','Richard Branson','5','Book','5551'),
    ('DPR61A','Michael Jackson','11245.5','DVD-disc','1500'),
    ('FPR41A','Refridgerator','144.15','Furniture','15x16x100');