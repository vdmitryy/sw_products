-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июл 11 2019 г., 15:33
-- Версия сервера: 5.7.24
-- Версия PHP: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `sw_products`
--

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `SKU` varchar(20) NOT NULL,
  `Name` varchar(150) NOT NULL,
  `Price` float(10,2) NOT NULL,
  `Type` enum('Furniture','DVD-disc','Book') NOT NULL,
  `Details` varchar(20) NOT NULL,
  `Created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`SKU`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`SKU`, `Name`, `Price`, `Type`, `Details`, `Created_at`) VALUES
('FPR11A', 'Chair', 115.10, 'Furniture', '15x552x51', '2019-07-11 18:33:02'),
('DPR11A', '50-cent - PIMP', 512.00, 'DVD-disc', '14500', '2019-07-11 18:33:02'),
('FPR21A', 'TV', 144.56, 'Furniture', '15x12x51', '2019-07-11 18:33:02'),
('DPR21A', 'Michael Jackson', 55245.50, 'DVD-disc', '1500', '2019-07-11 18:33:02'),
('BPR11A', 'About neighbours', 551.00, 'Book', '151', '2019-07-11 18:33:02'),
('BPR21A', 'Read it', 5.00, 'Book', '5551', '2019-07-11 18:33:02'),
('BPR31A', 'About me', 155.60, 'Book', '1100', '2019-07-11 18:33:02'),
('DPR31A', 'Wonderland', 25.40, 'DVD-disc', '500', '2019-07-11 18:33:02'),
('FPR31A', 'Sofa', 5555.00, 'Furniture', '515x5x100', '2019-07-11 18:33:02'),
('DPR41A', 'NIRVANA', 25.40, 'DVD-disc', '500', '2019-07-11 18:33:02'),
('DPR51A', '50-cent - IN DA CLUB', 52.00, 'DVD-disc', '14500', '2019-07-11 18:33:02'),
('BPR41A', 'Richard Branson', 5.00, 'Book', '5551', '2019-07-11 18:33:02'),
('DPR61A', 'Michael Jackson', 11245.50, 'DVD-disc', '1500', '2019-07-11 18:33:02'),
('FPR41A', 'Refridgerator', 144.15, 'Furniture', '15x16x100', '2019-07-11 18:33:02');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
