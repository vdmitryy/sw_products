<?php

namespace Products;

class Book extends Product {

    public $Weight;

    public static $det_fields = ["Weight"];
    public static $det_field_types = ["number"];
    public static $det_desc = "Please, provide Weight information in KG";
    public $det_name = "Weight";
    public $det_ext = "KG";

    protected $det_validation_rules = [
        "Weight" => ["minInt" => 1, "maxInt" => 100000000]
    ];
}