<?php

namespace Products;

class Furniture extends Product {

    public $Weight;
    public $Width;
    public $Length;

    public static $det_fields = ["Height", "Width", "Length"];
    public static $det_field_types = ["number", "number", "number"];
    public static $det_desc = "Please, provide dimensions";
    public $det_name = "Dimensions";
    public $det_ext = "cm";
    
    protected $det_validation_rules = [
        "Height" => ["minInt" => 10, "maxInt" => 100000000],
        "Width" => ["minInt" => 10, "maxInt" => 100000000],
        "Length" => ["minInt" => 10, "maxInt" => 100000000]
    ];
    
}