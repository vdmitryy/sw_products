<?php

namespace Products;

class Product
{
    protected static $db_table = "products";
    /**
     * All fields should correspond database fields.
     * Datetime field can be excluded, as there is a default value on create.
     */
    protected $fields = ['SKU','Name','Price','Type','Details'];
    protected static $database;

    /**
     * Primary key.
     * @var string
     */
    public $SKU;

    /**
     * @var string
     */
    public $Name;

    /**
     * @var float
     */
    public $Price;
    
    /**
     * enum('Furniture','DVD-disc','Book'). List can be extenden
     * only with changes in database.
     * @var string
     */
    public $Type;

    /**
     * Special features of Product (e.g. Size, Weight)
     * @var string
     */
    public $Details;
 
    /**
     * Errors after validation
     * @var string[]
     */
    public $errors = [];

    /**
     * As $this->Details can be composed from several parametres
     * (e.g. Dimensions: width, height, depth in format WxHxD)
     * the following parametres should be filled inside the child class.
     * --------- DETAILS START ---------
     */

    /**
     * Field to be merged into details field.
     * @var string[]
    */
    protected static $det_fields = [];
    
    /**
     * Type of fields for input validation.
     * @var string[] ['number','text']
    */
    protected static $det_field_types = [];

    /**
     * Appears in creation form
     * @var string
     */
    protected static $det_desc;

    /**
     * What $this->Details field represents
     * e.g. Width, Dimensions, Weight
     * @var string
     */
    protected $det_name;

    /**
     * Measurement of $this->Details
     * @var string
     */
    protected $det_ext;

    /**
     * Delimiter in case of many fields
     * @var string
     */
    protected $det_glue = "x";
    /**
     * --------- DETAILS END ---------
     */



    public function __construct($args = [])
    {
        foreach($this->fields as $field){
            $this->$field = (array_key_exists($field, $args)) ? $args[$field] : '';
        }
        foreach(static::$det_fields as $field){
            $this->$field = $args[$field] ?? "";
        }
    }

    /**
     * There is no need to check Details field
     * as it is being composed from other fields,
     * which are vaildated according to
     * $det_validation_rules from child class.
     * 
     * Options: ["min" => int]
     *          ["max" => int]
     *          ["minInt" => int]
     *          ["maxInt" => int]
     *          ["inArray" => string[]]
     */
    protected $validation_rules = [
        "SKU" => ["min" => 2, "max" => 20],
        "Name" => ["min" => 2, "max" => 150],
        "Price" => ["minInt" => 0, "maxInt" => 100000000],
        "Type" => ["inArray" => ['Furniture','DVD-disc','Book']]
    ];

    /**
     * should be defined in child class
     */
    protected $det_validation_rules = [];

    public function getDetailsName()
    {
        return $this->det_name ?? "";
    }

    public function getDetailsExt()
    {
        return $this->det_ext ?? "";
    }

    /**
     * implementation of active record pattern
     */

    /**
     * Database is set from Parent class so all
     * children can use it.
     * @param mysqli
     * @return void
     */
    public static function set_database($database)
    {
        self::$database = $database;
    }

    public static function db_query($sql)
    {
        $result = self::$database->query($sql);
        if (self::$database->error) exit("Error : DB query failed");
        return $result;
    }
    
    /**
     * find_all
     *
     * @param  string $order_by
     * @return Product[] Array of product children
     */
    public static function find_all($order_by = "Created_at")
    {
        $sql = "SELECT * FROM " . static::$db_table;
        $sql .= " ORDER BY ". $order_by ." DESC;";
        $result = static::db_query($sql);
        $product_objs = [];
        while($args = $result->fetch_assoc()){
            // instantiating classes according to their types
            // new object is Product's child
            $class = string_to_class($args["Type"]);
            $product_objs[] = new $class($args);
        }
        return $product_objs;
    }

    public function checkUniqueField($fieldName, $fieldValue)
    {
        $sql = "SELECT {$fieldName} FROM ". static::$db_table ." WHERE {$fieldName} = '";
        $sql .= self::$database->real_escape_string($fieldValue) ."';";
        return static::db_query($sql)->num_rows;
    }

    public function display_errors(){
        $error_block = '';
        if(sizeof($this->errors)){
            $error_block .= '<div class="errors grid vertical-grid">';
                foreach($this->errors as $error) {
            $error_block .= '<div class="error">'. $error .'</div>';
            }
            $error_block .= '</div>';
        }
        return $error_block;
    }

    /**
     * validate
     * Saves all errors to $this->errors;
     * 
     * @return void
     */
    public function validate()
    {
        $this->errors = [];
        // check for unique Primary Key
        if($this->checkUniqueField("SKU",$this->SKU))
            $this->errors[] = "Product with this SKU already exists";
        
        // take all fields and all validation rules from both
        // parent and child classes
        $all_fields = array_merge($this->fields, static::$det_fields);
        $all_validation_rules = array_merge($this->validation_rules, $this->det_validation_rules);

        // obligatory check on non-empty + additional checks from
        // validation rules
        foreach($all_fields as $field){
            if (isEmpty($this->$field)) $this->errors[] = $field . " should not be empty";
            $field_validation_rules = $all_validation_rules[$field] ?? [];
            foreach($field_validation_rules as $rule => $value){
                switch ($rule){
                    case "min":
                        if (!minLength($this->$field, $value))
                            $this->errors[] = $field . " should not be less than ". $value ." characters in length";
                        break;
                    case "max":
                        if (!maxLength($this->$field, $value))
                            $this->errors[] = $field . " should be not be less than ". $value ." characters in length";
                        break;
                    case "minInt":
                        if (!minInt((int)$this->$field, (int)$value))
                            $this->errors[] = $field . " should not be less than ". $value;
                        break;
                    case "maxInt":
                        if (!maxInt((int)$this->$field, (int)$value))
                            $this->errors[] = $field . " should be not be less than ". $value;
                        break;
                    case "inArray":
                        if (!in_array($this->$field, $value))
                            $this->errors[] = $field . $this->$field . " should be one of " . join(", ",$value);
                        break;
                    default:
                        break;
                }
            }
        }
    }
    
    /**
     * save 
     * Converts detail fields into $this->$Details, so
     * the object fully corresponds to database table.
     * 
     * Validates all fields of the object. 
     * 
     * Saves the object to the database. 
     *
     * @return Product record from db
     */
    public function save()
    {
        // for child classes combine values from static::$det_fields
        // (additional fields) into $this->Details property
        $values = [];
        foreach(static::$det_fields as $field){
            $values[] = $this->$field;
        }
        $this->Details = join($this->det_glue, $values);

        
        // validation using $this->validation_rules
        $this->validate();
        if(!empty($this->errors)){
            return false;
        }
        $sql = "INSERT INTO " . static::$db_table;
        $sql .= " (" . join(", ",$this->fields);
        $sql .= ") VALUES ("; 
        foreach($this->fields as $field){
            // escape characters before inserting record into db
            $sql .="'". self::$database->real_escape_string($this->$field);
            if($field != end($this->fields)) $sql .= "', ";
        }
        $sql .= "');";

        $result = static::db_query($sql);
        
        $_SESSION["messages"]["New product was created!"] = "success";
        return $result;
    }

    /**
     * getDetailsFormFields
     * Takes $det_fields of child object and generates
     * block of code with input fields.
     * Can be called without instantianting the object, what
     * was necessarry for AJAX.
     *
     * @return string 
     */
    public static function getDetailsFormFields()
    {
        $form_fields = '<div class="wrapper">'; 
        foreach(static::$det_fields as $i => $field) {
            $value = $_SESSION["product"][$field] ?? "";

            $form_fields .= '<div class="form-group">';
            $form_fields .= '<label for="'. $field . '">'. $field . '</label>';
            $form_fields .= '<input type="'. static::$det_field_types[$i] . '" ';
            $form_fields .= 'name="product['. $field . ']" id="'. $field . '" ';
            $form_fields .= 'value="'. $value  .'" required /></div>';
        }
        $form_fields .= '<div class="details">'. static::$det_desc . '</div></div>';
        return $form_fields;
    }

    /**
     * delete single db record outside the class
     *
     * @param  mixed $PK value
     * @param  mixed $column_name PK column name
     *
     * @return void
     */
    public static function delete($PK, $column_name = "SKU")
    {
        $sql = "DELETE FROM ". static::$db_table ." WHERE {$column_name} = {$PK};";
        $result = self::db_query($sql);
        return $result;
    }

    public static function deleteMany($PK_array = [], $column_name = "SKU")
    {
        // sanitize primary keys 
        $good_pk_array = array_map( function($key) {
            return htmlspecialchars(self::$database->real_escape_string($key));
        } ,array_keys($PK_array));

        // sql statement
        $sql = "DELETE FROM ". static::$db_table ." WHERE {$column_name} in ('";
        $sql .= join("', '",$good_pk_array) . "');";
        $result = static::db_query($sql);

        // set message
        $_SESSION["messages"]["Selected products were deleted!"] = "danger";
        return $result;
    }

}