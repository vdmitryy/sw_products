<?php

namespace Products;

class DVDdisc extends Product {

    public $Size;

    public static $det_fields = ["Size"];
    public static $det_field_types = ["number"];
    public static $det_desc = "Please, provide Size informaztion in MB";
    public $det_name = "Size";
    public $det_ext = "MB";

    protected $det_validation_rules = [
        "Size" => ["minInt" => 100, "maxInt" => 100000000]
    ];

}