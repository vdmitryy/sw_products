<?php 

// defining paths
define("PRIVATE_PATH", dirname(__FILE__));
define("SHARED_PATH", PRIVATE_PATH . "/shared");
define("ROOT_PATH", dirname(PRIVATE_PATH));
define("PUBLIC_PATH", ROOT_PATH . "/public");

// path for linking scripts, styling, for images
$public_end = strpos($_SERVER['SCRIPT_NAME'], '/public') + 7;
$doc_root = substr($_SERVER['SCRIPT_NAME'], 0, $public_end);
define("WWW_ROOT", $doc_root);

// database credentials
include_once(PRIVATE_PATH . "/db_credentials.php");

// including all necessary files
include_once(PRIVATE_PATH . "/functions/functions.php");
include_once(PRIVATE_PATH . "/functions/db_functions.php");

// autoloading classes using composer autoload
include_once(ROOT_PATH . "/vendor/autoload.php");

// enable session
session_start();

// tighten database to Parent class of all products
$db = db_connect();
Products\Product::set_database($db);