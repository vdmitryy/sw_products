## Installation
1. navigate to www directory of XAMPP/WAMP
2. `git clone https://vdmitryy@bitbucket.org/vdmitryy/sw_products.git`
3. `cd sw_products`
4. `composer install`
5. `npm install`
6. open MySQL, create a new database according to your credentials inside __/private/db_credentials.php__ 
7. create a new user for the database, provide it with permissions to create / read / update / delete
8. import file to the database from __private/database/sw_products.sql__
9. in browser, navigate to public folder of the project
10. Enjoy!

#### Notes
* composer was used for class autoloading
* npm for scss + browserSync + gulp configuration

## Assignment
Hello!
We are happy our company took your attention, however it is not the only thing we are
expecting from you. We have created a trial for you, to prove your bravery.
General requirements:
* PHP: ^7.0, plain classes, no frameworks, OOP approach
* jQuery: optional
* jQuery-UI: prohibited
* Bootstrap - optional
* SASS - advantage
* MySQL: ^5.6 - obligatory
* PSR-1/2 - optional

Code should be shared as bitbucket repository that is shared with a user whose email is as
follows: hr@scandiweb.com.

### Task description:
The expected outcome is 2 separate pages for:
1. product list
2. product add

1. Product list should list all existing product and details, like:
    * SKU (unique for each product)
    * Name
    * Price

Also each product type has special attribute, which we expect you would be able to
display as well (one of based on type):
    * Size (in MB) for DVD-disc
    * Weight (in Kg) for Book
    * Dimensions (HxWxL) for Furniture

An advantage would be implementation of the optional feature: mass delete action,
implemented as checkboxes next to each product.
Here is an example of product list page:
2. Product add page should display a form, with following fields
a. SKU
    * Name
    * Price
    * Type switcher (buttons for each type)
    * Special attribute [please note: the form should be dynamically changed when
type is switched]

Additional notes:
- Special attribute should have a description with helpful information, related to its type
ex.: “Please provide dimensions in HxWxL format”, when type: Furniture is selected.
- Utilize OOP principles to handle differences in type logic/behavior.
- Meeting PSR standards is an advantage (https://www.php-fig.org)
Additional requirements:
- All fields are mandatory for submission.
- Avoid using conditional statements for handling differences in product types.

An advantage would be fields value (or format whether suitable) validation.