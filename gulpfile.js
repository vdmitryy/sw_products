const gulp = require('gulp');
const concat = require('gulp-concat');
const scss = require('gulp-sass');
const browserSync = require('browser-sync');
const connect = require("gulp-connect-php");

function css() {
    return gulp.src('./private/assets/scss/*.scss')
        .pipe(scss().on('error', scss.logError))
        .pipe(gulp.dest('./public/css/'))
        .pipe(browserSync.stream());
}

function js() {
    return gulp.src([
        './node_modules/jquery/dist/jquery.min.js',
        './private/assets/js/*.js'
    ])
        .pipe(concat('main.js'))
        .pipe(gulp.dest('./public/js/'))
        .pipe(browserSync.stream());
}

function watch() {
    connect.server({}, () => {
        browserSync({
            proxy: '127.0.0.1:8000'
        });
    });
    gulp.watch('./private/assets/scss/*.scss').on('change', css);
    gulp.watch('./private/assets/js/*.js').on('change', gulp.series(js,browserSync.reload)) ;
    gulp.watch(['./public/*.html','./public/*.php','./private/shared/**/*.php']).on('change', browserSync.reload);
}

exports.default = watch;