<?php 
    include_once('./../private/initialize.php'); 

    $title = "Product list";
    include(SHARED_PATH . "/header.php");

    if(is_post_request()){
        // no need to sanitize them as they are already in DB,
        // user can not affect the input of this data
        $product_indexes = $_POST["products"] ?? [];

        // should perform an action if there is more then one
        // product chosen
        if(sizeof($product_indexes) > 0) {
            switch ($_POST["product_action"] ?? "") {
                case "massDelete": 
                    $result = Products\Product::deleteMany($product_indexes);
                    break;
                // additional actions can be definied here
                default:
                    break;
            }
            if ($result) {
                redirect_to("/index.php");
            }
        }
    }
?>

<main>
    <form action="<?php echo make_URL("/index.php"); ?>" method="POST">
        <header>
            <div class="nav-left">
                Product List
            </div>
            <div class="nav-right">
                <a href="<?php echo make_url('create.php') ?>" class="button">Create</a>
                <fieldset class="main-actions">
                    <select name="product_action" id="product_action">
                        <option value="massDelete" selected>Mass delete</option>
                    </select>
                    <input type="submit" value="Delete">
                </fieldset>
            </div>
        </header>
        <hr>

        <?php echo show_messages(); ?>

        <div class="grid products-grid">

            <?php foreach(Products\Product::find_all() as $product){ ?>  
                <div class="product" onclick="toggleCheckbox(this)">
                    <fieldset class="checkbox">
                        <input type="checkbox" name="products[<?php echo $product->SKU; ?>]">
                    </fieldset>
                    <div class="grid vertical-grid text-center">
                        <div><?php echo $product->SKU; ?></div>
                        <div class="name"><?php echo $product->Name; ?></div>
                        <div><?php echo $product->Price; ?> &euro;</div>
                        <div><?php echo $product->Type; ?></div>
                        <div><?php echo $product->getDetailsName();
                        echo " : " . $product->Details . " ";
                        echo $product->getDetailsExt(); ?></div>
                    </div>
                </div>
            <?php } ?>

        </div>
    </form>
</main>

<?php include(SHARED_PATH . "/footer.php") ?>