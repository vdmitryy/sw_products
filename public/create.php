<?php 
    include_once('./../private/initialize.php');

    $title = "Product list";
    include(SHARED_PATH . "/header.php");

    if(is_post_request()) {
        $args = sanitize_input($_POST["product"]);
        $_SESSION["product"] = $args;

        // create a child product class, based on selected
        // type, and save it to database
        $class = string_to_class($args["Type"] ?? "");
        $product = new $class($args);
        $result = $product->save();
        if ($result) {
            unset($_SESSION["product"]);
            redirect_to("/index.php");
        }
    }
    // if a user has just started creating product, then
    // form is being supplied with the parent oclass object
    // that has empty strings as properties
    if(!isset($product)) $product = new Products\Product();
?>

<main>
    <form action="<?php echo make_url("/create.php"); ?>" method="POST">
        <header>
            <div class="nav-left">
                Product Create
            </div>
            <div class="nav-right">
                <a href="<?php echo make_url('index.php') ?>" class="button">Index</a>
                <fieldset class="main-actions">
                    <input type="submit" value="Create">
                </fieldset>
            </div>
        </header>
        <hr>
        
        <?php echo $product->display_errors() ?>

        <div class="el-center">
            <fieldset class="create-fields grid vertical-grid">
                <div class="form-group">
                    <label for="SKU">SKU</label>
                    <input type="text" name="product[SKU]" id="SKU" value="<?php echo $product->SKU; ?>" required />
                </div>
                <div class="form-group">
                    <label for="Name">Name</label>
                    <input type="text" name="product[Name]" id="Name" value="<?php echo $product->Name; ?>" required />
                </div>
                <div class="form-group">
                    <label for="Price">Price</label>
                    <input type="number" name="product[Price]" id="Price" value="<?php echo $product->Price; ?>" required />
                </div>
                <div class="form-group">
                    <label for="typeSelector">Type</label>
                    <select name="product[Type]" id="typeSelector" onchange="getDetailsFields(this.value)" required>
                        <option value="" <?php echo ($product->Type == "")? "selected" : ""; ?> ></option>
                        <option value="Book" <?php echo ($product->Type == "Book")? "selected" : ""; ?> >Book</option>
                        <option value="DVD-disc" <?php echo ($product->Type == "DVD-disc")? "selected" : ""; ?> >DVD-disc</option>
                        <option value="Furniture" <?php echo ($product->Type == "Furniture")? "selected" : ""; ?> >Furniture</option>
                    </select>
                </div>
                <div class="form-additional-group grid vertical-grid" id="detailed_info">
                </div>
            </fieldset>
        </div>
    </form>
</main>

<?php include(SHARED_PATH . "/footer.php") ?>